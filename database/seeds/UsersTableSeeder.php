<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $licencias = ['APPA ', 'PPA ', 'PCA ', 'IV ', 'PC1 ', 'TLA '];

        DB::table('users')->insert([
            'name' => 'ADMINISTRADOR BAFLIGHT',
            'email' => 'admin@baflight.com',
            'licencia' => 'ADMINISTRADOR',
            'password' => bcrypt('ff34hh567'),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        for($i = 1; $i < 200; $i++)
        {
            DB::table('users')->insert([
                'name' => strtoupper($faker->name),
                'email' => $faker->unique()->safeEmail,
                'licencia' => $licencias[rand(0, 5)] . rand(20000000, 43000000),
                'password' => bcrypt('ff34hh567'),
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }
}
