<?php

// Auth
Auth::routes(['verify' => false, 'register' => false, 'reset' => false]);

// Dashboard
Route::get('/', 'HomeController@index');
